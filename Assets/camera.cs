using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera : MonoBehaviour
{
    public Camera CentralCamera;
    public Camera FirstPersonCamera;
    public Camera ZonaCamera;
    public Camera MoufleCamera;


    // Start is called before the first frame update
    void Start()
    {
        CentralCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        FirstPersonCamera = GameObject.Find("FirstPersonCamera").GetComponent<Camera>();
        ZonaCamera = GameObject.Find("ZonaCamera").GetComponent<Camera>();
        MoufleCamera = GameObject.Find("MoufleCamera").GetComponent<Camera>();
        //camera par defaut
        ShowCentralCameraView();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Keypad1)){
            ShowFirstPersonCameraView();
        }

        if (Input.GetKeyDown(KeyCode.Keypad2)){
            ShowCentralCameraView();
        }

        if (Input.GetKeyDown(KeyCode.Keypad3)){
            ShowZonaCameraView();
        }
        if (Input.GetKeyDown(KeyCode.Keypad4)){
            ShowMoufleCameraView();
        }
    }

    public void ShowFirstPersonCameraView() {
        ZonaCamera.enabled = false;
        CentralCamera.enabled = false;
        MoufleCamera.enabled = false;
        FirstPersonCamera.enabled = true;
    }
    public void ShowCentralCameraView() {
        ZonaCamera.enabled = false;
        FirstPersonCamera.enabled = false;
        MoufleCamera.enabled = false;
        CentralCamera.enabled = true;
    }
    
    public void ShowZonaCameraView() {
        FirstPersonCamera.enabled = false;
        CentralCamera.enabled = false;
        MoufleCamera.enabled = false;
        ZonaCamera.enabled = true;
    }

    public void ShowMoufleCameraView() {
        FirstPersonCamera.enabled = false;
        CentralCamera.enabled = false;
        ZonaCamera.enabled = false;
        MoufleCamera.enabled = true;
    }
}
